package exercise.android.reemh.todo_items;

import junit.framework.TestCase;

import org.junit.Assert;
import org.junit.Test;

import java.util.Vector;

public class TodoItemsHolderImplTest extends TestCase {

  @Test
  public void test_when_addingTodoItem_then_callingListShouldHaveThisItem(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();
    assertEquals(0, holderUnderTest.getCurrentItems().size());

    // test
    holderUnderTest.addNewInProgressItem("do shopping");

    // verify
    assertEquals(1, holderUnderTest.getCurrentItems().size());
  }

  @Test
  public void test_when_deletingTodoItem_then_SizeOfListShouldUpdate(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();
    assertEquals(0, holderUnderTest.getCurrentItems().size());

    // test
    holderUnderTest.addNewInProgressItem("do shopping");
    Vector<TodoItem> items = holderUnderTest.getCurrentItems();
    TodoItem itemToDelete = items.get(0);
    holderUnderTest.deleteItem(itemToDelete);

    // verify
    assertEquals(0, holderUnderTest.getCurrentItems().size());
  }

  @Test
  public void test_when_ChangingItemStateToDone_theHolderIsUpdatedCorrectly(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();

    // test
    holderUnderTest.addNewInProgressItem("do shopping");
    Vector<TodoItem> items = holderUnderTest.getCurrentItems();
    TodoItem itemDone = items.get(0);
    holderUnderTest.markItemDone(itemDone);
    items = holderUnderTest.getCurrentItems();
    itemDone = items.get(0);

    // verify
    assertTrue(itemDone.GetIsDone());
  }

  @Test
  public void test_when_ChangingItemStateToInProgress_theHolderIsUpdatedCorrectly(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();

    // test
    holderUnderTest.addNewInProgressItem("do shopping");
    Vector<TodoItem> items = holderUnderTest.getCurrentItems();
    TodoItem itemDone = items.get(0);
    holderUnderTest.markItemDone(itemDone);
    items = holderUnderTest.getCurrentItems();
    itemDone = items.get(0);
    holderUnderTest.markItemInProgress(itemDone);
    items = holderUnderTest.getCurrentItems();
    itemDone = items.get(0);

    // verify
    assertFalse(itemDone.GetIsDone());
  }

  @Test
  public void test_when_AddingAFewItems_theHolderIsUpdatedCorrectly(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();

    // test
    holderUnderTest.addNewInProgressItem("do shopping");
    holderUnderTest.addNewInProgressItem("clean the house");
    holderUnderTest.addNewInProgressItem("make lunch");
    holderUnderTest.addNewInProgressItem("do homework");


    // verify
    assertEquals(4, holderUnderTest.getCurrentItems().size());
  }

  @Test
  public void test_when_AddingANewItem_theDescriptionIsUploadedCorrectly(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();
    holderUnderTest.addNewInProgressItem("do shopping");

    // test
    TodoItem itemToCheck = holderUnderTest.getCurrentItems().get(0);

    // verify
    assertEquals("do shopping", itemToCheck.GetDescription());
  }


  @Test
  public void test_when_AddingANewItem_theStateIsUploadedCorrectly(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();
    holderUnderTest.addNewInProgressItem("do shopping");

    // test
    TodoItem itemToCheck = holderUnderTest.getCurrentItems().get(0);

    // verify
    assertFalse(itemToCheck.GetIsDone());
  }

  @Test
  public void test_when_AddingANewItem_itIsPlacedInTheBeginning(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();
    holderUnderTest.addNewInProgressItem("do shopping");
    holderUnderTest.addNewInProgressItem("clean the house");

    // test
    TodoItem itemToCheck = holderUnderTest.getCurrentItems().get(0);

    // verify
    assertEquals("clean the house", itemToCheck.GetDescription());
  }

  @Test
  public void test_when_MarkingItemDone_itIsPlacedAtTheEnd(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();
    holderUnderTest.addNewInProgressItem("do shopping");
    holderUnderTest.addNewInProgressItem("clean the house");

    // test
    TodoItem itemToCheck = holderUnderTest.getCurrentItems().get(0);
    holderUnderTest.markItemDone(itemToCheck);

    Vector<TodoItem> allItems = holderUnderTest.getCurrentItems();
    // verify
    assertEquals("clean the house", allItems.get(allItems.size()-1).GetDescription());
  }

  @Test
  public void test_when_DeletingAnItem_ItDoesNotExist_inTheList(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();
    holderUnderTest.addNewInProgressItem("do shopping");
    holderUnderTest.addNewInProgressItem("clean the house");
    holderUnderTest.addNewInProgressItem("wash the dishes");
    holderUnderTest.addNewInProgressItem("go to the grocery");
    holderUnderTest.addNewInProgressItem("cook food");


    // test
    TodoItem itemDone1 = holderUnderTest.getCurrentItems().get(0);
    holderUnderTest.markItemDone(itemDone1);
    TodoItem itemDone2 = holderUnderTest.getCurrentItems().get(3);
    holderUnderTest.markItemDone(itemDone2);

    TodoItem itemToDelete = holderUnderTest.getCurrentItems().get(0);
    holderUnderTest.deleteItem(itemToDelete);


    Vector<TodoItem> allItems = holderUnderTest.getCurrentItems();
    for(TodoItem item : allItems) {
      // verify
      Assert.assertNotEquals("go to the grocery", item.GetDescription());
    }
  }

  @Test
  public void test_when_FinishingAllTodos_allStateAreCorrect(){
    // setup
    TodoItemsHolderImpl holderUnderTest = new TodoItemsHolderImpl();
    holderUnderTest.addNewInProgressItem("do shopping");
    holderUnderTest.addNewInProgressItem("clean the house");
    holderUnderTest.addNewInProgressItem("wash the dishes");
    holderUnderTest.addNewInProgressItem("go to the grocery");
    holderUnderTest.addNewInProgressItem("cook food");


    // test
    TodoItem itemDone1 = holderUnderTest.getCurrentItems().get(0);
    holderUnderTest.markItemDone(itemDone1);
    TodoItem itemDone2 = holderUnderTest.getCurrentItems().get(0);
    holderUnderTest.markItemDone(itemDone2);
    TodoItem itemDone3 = holderUnderTest.getCurrentItems().get(0);
    holderUnderTest.markItemDone(itemDone3);
    TodoItem itemDone4 = holderUnderTest.getCurrentItems().get(0);
    holderUnderTest.markItemDone(itemDone4);
    TodoItem itemDone5 = holderUnderTest.getCurrentItems().get(0);
    holderUnderTest.markItemDone(itemDone5);

    Vector<TodoItem> allItems = holderUnderTest.getCurrentItems();
    for(TodoItem item : allItems) {
      // verify
      assertTrue(item.GetIsDone());
    }
  }

}
package exercise.android.reemh.todo_items

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.annotation.VisibleForTesting
import androidx.recyclerview.widget.RecyclerView
import java.util.*

class TodoAdapter: RecyclerView.Adapter<TodoView>() {

    var onItemClickCallback: ((TodoItem) -> Unit) ?= null
    var onDeleteClickCallback: ((TodoItem) -> Unit) ?= null
    var onEditClickCallback: ((TodoItem) -> Unit) ?= null


    private val _todoItems: Vector<TodoItem> = Vector()

    fun setTodo(item: Vector<TodoItem>) {
       _todoItems.clear()
       _todoItems.addAll(item)
       notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return _todoItems.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoView {
        val context = parent.context
        val view = LayoutInflater.from(context)
                .inflate(R.layout.row_todo_item, parent, false)
        return TodoView(view)
    }


    override fun onBindViewHolder(holder: TodoView, position: Int) {
        val todo = _todoItems[position]
        if(todo.GetIsDone()) {
            holder.description.apply {
                paintFlags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            }
        }
        else {
            holder.description.apply {
                paintFlags = 0
            }
        }
        holder.state.isChecked = todo.GetIsDone()
        holder.description.text = todo.GetDescription()
        holder.state.setOnClickListener {
            val callback = onItemClickCallback ?: return@setOnClickListener
            callback(todo)
        }

        holder.delete.setOnClickListener {
            val callback = onDeleteClickCallback ?: return@setOnClickListener
            callback(todo)
        }

        holder.description.setOnClickListener {
            val callback = onEditClickCallback ?: return@setOnClickListener
            callback(todo)
        }
    }
}


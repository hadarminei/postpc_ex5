package exercise.android.reemh.todo_items;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Set;
import java.util.UUID;
import java.util.Vector;

public class TodoItemsHolderImpl implements TodoItemsHolder  {

  private final Vector<TodoItem> inProgressItems = new Vector<>();
  private final Vector<TodoItem> doneItems = new Vector<>();
  private final SharedPreferences sp;
  private final Context context;

  public TodoItemsHolderImpl(Context context) {
    this.context = context;
    sp = context.getSharedPreferences("local_db_items", context.MODE_PRIVATE);
    initializeFromSp();
  }

  private void initializeFromSp() {
    Set<String> keys = sp.getAll().keySet();
    for(String key : keys) {
      String stringTodo = sp.getString(key, null);
      TodoItem item = TodoItem.stringToTodo(stringTodo);
      if (item != null) {
        if (item.GetIsDone()) {
          doneItems.add(item);
        } else {
          inProgressItems.add(item);
        }
      }
    }
  }

  @Override
  public Vector<TodoItem> getCurrentItems() {
    Vector<TodoItem> allItems = new Vector<>();
    allItems.addAll(doneItems);
    allItems.addAll(inProgressItems);
    Collections.reverse(allItems);
    return allItems;
  }

  @Override
  public void addNewInProgressItem(String description) {
    if (!description.isEmpty()) {
      Date timeNow = Calendar.getInstance().getTime();
      String id = UUID.randomUUID().toString();
      TodoItem newItem = new TodoItem(id,false, description, timeNow.toString(), timeNow.toString());
      inProgressItems.add(newItem);

      SharedPreferences.Editor editor = sp.edit();
      editor.putString(newItem.getIdItem(), newItem.generateString());
      editor.apply();
      sendBroadcast();
    }
  }

  @Override
  public void markItemDone(TodoItem item) {
    TodoItem updatedItem = new TodoItem(item.getIdItem(), true, item.GetDescription(), item.GetCreationTime(),
            Calendar.getInstance().getTime().toString());
    if (inProgressItems.remove(item)) {
      doneItems.add(updatedItem);
      SharedPreferences.Editor editor = sp.edit();
      editor.putString(updatedItem.getIdItem(), updatedItem.generateString());
      editor.apply();
      sendBroadcast();
    }
  }

  @Override
  public void markItemInProgress(TodoItem item) {
    TodoItem updatedItem = new TodoItem(item.getIdItem(), false, item.GetDescription(), item.GetCreationTime(),
            Calendar.getInstance().getTime().toString());
    if (doneItems.remove(item)) {
      inProgressItems.add(updatedItem);
      SharedPreferences.Editor editor = sp.edit();
      editor.putString(updatedItem.getIdItem(), updatedItem.generateString());
      editor.apply();
      sendBroadcast();
    }
  }

  @Override
  public void deleteItem(TodoItem item) {
    if (!item.GetIsDone()) {
      inProgressItems.remove(item);
    } else {
      doneItems.remove(item);
    }
    SharedPreferences.Editor editor = sp.edit();
    editor.remove(item.getIdItem());
    editor.apply();
    sendBroadcast();
  }

  public void editDescription(TodoItem item, String newDescription) {
    TodoItem updatedItem = new TodoItem(item.getIdItem(),false, item.GetDescription(), item.GetCreationTime(),
            Calendar.getInstance().getTime().toString());
    if (!item.GetIsDone()) {
      inProgressItems.remove(item);
      inProgressItems.add(updatedItem);
    } else {
      doneItems.remove(item);
      doneItems.add(updatedItem);
    }
    SharedPreferences.Editor editor = sp.edit();
    editor.putString(updatedItem.getIdItem(), updatedItem.generateString());
    editor.apply();
    sendBroadcast();
  }

  public void sendBroadcast() {
    Intent broadcast = new Intent("Todo_changed");
    broadcast.putExtra("todoItems", getCurrentItems());
    context.sendBroadcast(broadcast);
  }

};

package exercise.android.reemh.todo_items

import android.view.View
import android.widget.CheckBox
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class TodoView(view: View): RecyclerView.ViewHolder(view){
    val state: CheckBox = view.findViewById(R.id.check_box)
    val description: TextView = view.findViewById(R.id.task_text)
    val delete: ImageButton = view.findViewById(R.id.delete)
}
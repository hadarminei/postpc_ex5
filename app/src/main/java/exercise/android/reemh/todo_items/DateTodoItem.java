package exercise.android.reemh.todo_items;

import java.util.Date;
import java.util.HashMap;

public class DateTodoItem {
    private HashMap<String, Integer> months = new HashMap<String, Integer>() {{
        put("January", 1);
        put("February", 2);
        put("March", 3);
        put("April", 4);
        put("May", 5);
        put("June", 6);
        put("July", 7);
        put("August", 8);
        put("September", 9);
        put("October", 10);
        put("November", 11);
        put("December", 12);
    }};
    public int month;
    public int day;
    public int hour;
    public int minutes;

    DateTodoItem(String date) {
        String[] seperatedDate = date.split(" ");
        try {
            this.month = months.get(seperatedDate[0]);
            this.day = Integer.parseInt(seperatedDate[1].substring(0, seperatedDate[1].length() - 2));
            String hourAndMinutes = seperatedDate[3];
            String[] parseHourMinutes = hourAndMinutes.split(":");
            this.hour = Integer.parseInt(parseHourMinutes[0]);
            this.minutes = Integer.parseInt(parseHourMinutes[1]);
        }
        catch (Exception e) {
            System.out.println("exception");
        }
    }
}

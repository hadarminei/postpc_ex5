I pledge the highest level of ethical principles in support of academic excellence.
I ensure that all of my work reflects my own abilities and not those of someone else.
Hadar Minei
___________________________________________________________________________________________________

We didn't define any UX flow to let users edit a description on an existing TODO item.
Which UX flow will you define?
In your response notice the following:
1. how easy is it for users to figure out this flow in their first usage? (for example, a glowing 
button is more discoverable then a swipe-left gesture)
2. how hard to implement will your solution be?
3. how consistent is this flow with regular "edit" flows in the Android world?

The most basic UX that comes to mind and is also the most intuitive is adding an edit button with a
little logo of a pencil (the usual one). This button will act the same way as the delete button, and
will also be placed next to it. When the user pushes the button, the edit state will be brought to
action.
I think that this solution will be very easy to implement. First we will need to add to our todos_rows
xml the new button with the image, and give it a corresponding id, and after that we will add the 
functionality to the adapter and probably implement the on-click function in the Main Activity.
This solution is very consistent with the regular "edit" flow. On most of the apps which have this
kind of interaction we can find an edit button.